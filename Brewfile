cask_args appdir: "/Applications"

# ensure everything installed afterwards can depend on recent version of readline and openssl
brew "readline"
brew "openssl"

# python
brew "pyenv"
brew "pyenv-virtualenv"

# node
brew "nodejs"
brew "yarn"
brew "npm"

# a more recent version of java than the default
cask "java"

# git tools
brew "git"
cask "github"

# zsh shell, better shell and commandline experience
cask "iterm2" # best terminal app on mac
brew "zsh"
brew "zsh-completion"
brew "zsh-autosuggestions"
brew "zsh-syntax-highlighting"

# fast & accurate command line search
brew "fzf" # fuzzy finder
brew "ripgrep"

brew "ssh-copy-id" # copy ssh keys to remote servers
brew "tree"
brew "watch"
brew "tmux"

# http clients
brew "wget"
brew "curl"

# browsers
cask "google-chrome"
cask "firefox"
cask "opera"

# chat/communication
cask "slack"
cask "microsoft-teams"

# developer editors
cask "atom" # atom editor from github
cask "visual-studio-code"
brew "vim" # command line editor for working with text
cask "macvim" # vim in as mac native application

# developer tools
cask "docker" # docker4mac for working with images & containers
cask "postman" # developer tool for API's
cask "kaleidoscope" # great file diff UI
cask "devdocs" # devdocs.io desktop client for developer documentation a la Dash

# random tools
cask "awscli" # AWS CLI
cask "spectacle" # window management
cask "imageoptim" # optimize image size file per drag'n'drop
cask "kap" # capture screen as video
cask "the-unarchiver" # extract all kinds of file archives
cask "licecap" # record screen video as gif
cask "cheatsheet" # show shortcuts of current app
cask "tunnelblick"
